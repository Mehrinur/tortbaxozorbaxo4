package uz.yordam.atmdt.webserver;


import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;

public class MainVerticle extends AbstractVerticle {
    @Override
    public void start() {
        Router router=new MainRouter().getRouter(vertx);
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(8080);
//                        routerContext->routerContext
//                        .response()
//                        .end("salom")
//                )
//                .listen(8080);
    }
}

