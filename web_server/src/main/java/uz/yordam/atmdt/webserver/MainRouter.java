package uz.yordam.atmdt.webserver;

import io.vertx.core.Vertx;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;


public class MainRouter {
    private MainController mainController;
    private AlochiController alichiController;
    private AqilliyController aqilliyController;
    private OqituvchiController oqituvchiController;
private OquvchiController oquvchiController;
private YigindiController YigindiController;
private SevimliController SevimliController;
    private archaController archaController;
private  dynamicController dynamicController;
private  BolinmaController bolinmaController;
private  StipendiyaController stipendiyaController;
private IndexController indexController;
    public MainRouter(){
        mainController=new MainController();
       alichiController=new AlochiController();
       aqilliyController=new AqilliyController();
       oqituvchiController=new OqituvchiController();
       oquvchiController =new OquvchiController();
       SevimliController = new SevimliController();
       YigindiController=new YigindiController();
        archaController=new archaController();
        dynamicController=new dynamicController();
        bolinmaController=new BolinmaController();
        stipendiyaController=new StipendiyaController();
        indexController=new IndexController();
    }
    public Router getRouter(Vertx vertx){
        Router router=Router.router(vertx);
        router.route("/javob").handler(mainController::Javob);
        router.route("/beshchi").handler(alichiController::beshchi);
        router.route("/izox").handler(mainController::Izox);
        router.route("/alobaxo").handler(alichiController::alobaxo);
        router.route("/bilim").handler(oqituvchiController::bilim);
        router.route("/ziyoli").handler(oqituvchiController::ziyoli);
        router.route("/kitobxon").handler(aqilliyController::Kitobxon);
        router.route("/kitob").handler(aqilliyController::kitob);
        router.route("/zukko").handler(oquvchiController::Zukko);
        router.route("/bilimdon").handler(oquvchiController::bilimdon);
        router.route("/xontaxta").handler(SevimliController::Xontaxta);
        router.route("/joxonxurust").handler(SevimliController::Joxonxurust);

        // Router router=Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/stipendiya/uchlik").handler(stipendiyaController::Uchlik);
        router.route("/stipendiya/tortlik").handler(stipendiyaController::Tortlik);
        router.route("/stipendiya/beshlik").handler(stipendiyaController::Beshlik);
        router.route("/bolinma/:son1/:son2").handler(bolinmaController::bolinma);
        router.route("/yigindi/:son1/:son2").handler(YigindiController::yigindi);
        router.route("/archa/:son1/:son2").handler(archaController::metod1);
        router.route("/dinamik/:son1").handler(dynamicController::metod1);
        router.route("/Xindex/index").handler(indexController::index1);
        router.route().handler(this::page404);
        return router;
    }
    public  Router makeRouter(Vertx vertx){
        Router router= Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/yigindi/:son1/:son2").handler(YigindiController::yigindi);
        router.route("/bolinma/:son1/:son2").handler(bolinmaController::bolinma);
        router.route("/stipendiya/uchlik").handler(stipendiyaController::Uchlik);
        router.route("/stipendiya/tortlik").handler(stipendiyaController::Tortlik);
        router.route("/stipendiya/beshlik").handler(stipendiyaController::Beshlik);

        return  router;
    }

    public void page404(RoutingContext routingContext){
        routingContext
                .response()
                .setStatusCode(404)
                .end("xato");
    }
}

