package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class AlochiController {
    public void beshchi(RoutingContext routingContext)
    {
        routingContext.response()
                .end("alochi bolalarni oqituvchilar sevadi");
    }

    public void alobaxo(RoutingContext routingContext)
    {
        routingContext.response().end("ular 5 baxo oladilar.");
    }
}

